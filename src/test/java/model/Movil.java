package model;

public class Movil {
    String conductor;
    String distancia;
    String cuadrante;
    String ubicacion;
    Double curso;
    
    public String getConductor() {
        return conductor;
    }
    public void setConductor(String conductor) {
        this.conductor = conductor;
    }
    public String getDistancia() {
        return distancia;
    }
    public void setDistancia(String distancia) {
        this.distancia = distancia;
    }
    public String getCuadrante() {
        return cuadrante;
    }
    public void setCuadrante(String cuadrante) {
        this.cuadrante = cuadrante;
    }
    public String getUbicacion() {
        return ubicacion;
    }
    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }
    public Double getCurso() {
        return curso;
    }
    public void setCurso(Double curso) {
        this.curso = curso;
    }
}
