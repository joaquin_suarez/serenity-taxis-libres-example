package stepdefinitions;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.http.HttpStatus;
import org.assertj.core.api.SoftAssertions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import net.serenitybdd.junit.runners.SerenityParameterizedRunner;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.WithTag;
import net.thucydides.junit.annotations.Qualifier;
import net.thucydides.junit.annotations.UseTestDataFrom;
import status.CacheIntegrationStatus;

@RunWith(SerenityParameterizedRunner.class)
@WithTag("cacheintegration")
@UseTestDataFrom(value="src/test/resources/coordenadas.csv")
public class TaxisLibresCacheIntegrationStatus {
    
    public TaxisLibresCacheIntegrationStatus() {
        super();
    }

    private String latitud;                      
    private String longitud;                        
    private SoftAssertions softAssertions = new SoftAssertions();
    
    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }
    
    public String getLatitud() {
        return this.latitud;
    }

    public String getLongitud() {
        return this.longitud;
    }

    @Qualifier
    public String qualifier() {
        return "latitud "+latitud + " y longitud" + longitud;
    }

    @Steps
    CacheIntegrationStatus cacheIntegrationStatus;
    
    String url;
    
    @Before
    public void setup() {
        url = "http://213.136.84.4:8180/CacheIntegration/AddGetDispController?disp=get&latitud="+getLatitud()+"&longitud="+getLongitud()+"&ciudad=11001&nameSpace=REDAMARILLA";
    }
    
    @After
    public void assertAll() {
        softAssertions.assertAll();
    }
    
    //String url = String.format("http://213.136.84.4:8180/CacheIntegration/AddGetDispController?disp=get&latitud=%1$s&longitud=%2$s&ciudad=11001&nameSpace=REDAMARILLA", latitud, longitud);
    @Test
    public void estadoDeConexionServicio() {
        assertThat(cacheIntegrationStatus.obtenerStatusServicio(url)).isEqualTo(HttpStatus.SC_OK);
    }
    
    @Test
    public void verificarCuerpoRespuesta() {
        assertThat(cacheIntegrationStatus.obtenerCuerpoMensaje(url)).isEqualTo(true);
    }
    
    @Test
    public void verificarCantidadMoviles() {
        assertThat(cacheIntegrationStatus.obtenerCantidadDeMoviles(url)).isGreaterThan(0);
    }
    @Test
    public void allValidationsInOne() {
        softAssertions
            .assertThat(cacheIntegrationStatus.obtenerStatusServicio(url)).isEqualTo(HttpStatus.SC_OK);
        softAssertions
            .assertThat(cacheIntegrationStatus.obtenerCuerpoMensaje(url)).isEqualTo(true);
        softAssertions
            .assertThat(cacheIntegrationStatus.obtenerCantidadDeMoviles(url)).isGreaterThan(0);
    }
}
