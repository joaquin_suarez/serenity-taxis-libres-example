package helper;


public enum WebServiceEndPoints {
    TAXIS_LIBRES("http://213.136.84.4:8180/CacheIntegration/AddGetDispController?disp=get&latitud=4.6267362&longitud=-74.1081142&ciudad=11001&nameSpace=REDAMARILLA");

    private final String url;

    WebServiceEndPoints(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
