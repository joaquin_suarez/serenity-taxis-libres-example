package status;

import java.util.ArrayList;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import io.restassured.response.ResponseBody;
import model.Movil;
import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;


public class CacheIntegrationStatus {
    
    ArrayList<Movil> listaMoviles = new ArrayList<>();
    //String url = "http://213.136.84.4:8180/CacheIntegration/AddGetDispController?disp=get&latitud=4.6267362&longitud=-74.1081142&ciudad=11001&nameSpace=REDAMARILLA";
    
    @Step("Respuesta del servicio de cache integration")
    public int obtenerStatusServicio(String url) {
        return SerenityRest.get(url).getStatusCode();
    }
    
    @Step("Validacion del cuerpo del mensaje")
    public Boolean obtenerCuerpoMensaje(String url) {
        ResponseBody body = SerenityRest.get(url).getBody();
        return body.asString().contains("moviles");   
    }
    @Step("Verificar cantidad de moviles")
    public int obtenerCantidadDeMoviles(String url) {
        ResponseBody body = SerenityRest.get(url).getBody();
        String jsonBody = body.asString();
        JsonParser jsonParser = new JsonParser();
        JsonObject arrayFromString = jsonParser.parse(jsonBody).getAsJsonObject();
        return ((JsonArray) arrayFromString.get("moviles")).size();
    }
}   
